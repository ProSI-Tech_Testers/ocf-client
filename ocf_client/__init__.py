'''
Created on Feb 18, 2017

@author: Samir Khellaf

ocf_client: OCF client.

Copyright (C) 2017 Samir Khellaf <opensource@prosi-tech.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
'''

# Imports to make code work on both python 2 & 3
from __future__ import absolute_import, division, print_function
from six import class_types, integer_types, string_types, text_type, binary_type
from six.moves.urllib.parse import urlsplit, urlunsplit

# Imports required by the current module
from requests import request

# Logging
import logging
logging.basicConfig(level=logging.WARN)

class OcfClientError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        if isinstance(self.value, string_types): return self.value
        else: return repr(self.value)

class OcfClient(object):
    '''
    classdocs
    '''

    def __init__(
            self,
            url='',
            scheme='http',
            host='',
            port=80,
            path='/ocf/'):
        '''
        Constructor
        '''
        if url != None:
            url = urlsplit(url)
        elif host != None:
            url = urlsplit('//' + host)
        else:
            raise OcfClientError(
                    'At least either url or host should be provided to '
                    + 'OcfClient constructor')
        scheme = url.scheme or scheme
        host = url.hostname or host
        if not host:
            raise OcfClientError('Provided url does not contain a hostname')
        port = url.port or port
        if port: host += ':{}'.format(port)
        path = url.path or path
        if(url.query != '' or url.fragment != ''):
            raise OcfClientError(
                    'OCF url shall not contain neither a query nor a fragment')
        url_str = urlunsplit((scheme, host, path, '', ''))

        self.url = urlsplit(url_str)

    def Get(self, *args):
        '''
        Get OCF resource properties method
        Usage:
            ocf.Get(<resource_name>, [device], [query])
        or
            ocf.Get([resource], [query])

        '''
        # Check arguments
        if len(args) > 3:
            raise OcfClientError('Error Get requires at most 3 arguments')

        # Platform information of a device
        if args[0] == 'PlatformInformation':
            result = request('GET', self.url.geturl() + args[1] + '/oic/p')

        # Device information of a device
        elif args[0] == 'DeviceInformation':
            result = request('GET', self.url.geturl() + args[1] + '/oic/d')

        # Configuration information of a device
        elif args[0] == 'ConfigurationInformation':
            result = request('GET', self.url.geturl() + args[1] + '/oic/con')

        # Maintenance information of a device
        elif args[0] == 'MaintenanceInformation':
            result = request('GET', self.url.geturl() + args[1] + '/oic/mnt')

        # Resources in a device
        elif args[0] == 'Resources':
            result = request('GET', self.url.geturl() + args[1] + '/oic/res')

        # Properties of a resource
        else:
            result = request('GET', self.url.geturl() + args[0])

        # Not found
        if result.status_code == 404: return(None)

        # Error
        elif result.status_code >= 400:
            raise OcfClientError("Error getting '{0}': {1} ({2})".format(
                args[0], result.reason, result.status_code))
        return(result.json())
    
    def Set(self, resource, properties):
        '''
        Set OCF resource properties method
        '''
        # Check arguments
        if not isinstance(properties, dict):
            raise OcfClientError(
                'Invalid properties type! expecting a dictionary.')

        # This code is temporary, and for an implementation that does not
        # support incomplete properties setting
        result = self.Get(resource)
        for p in properties:
            if p in result:
                result[p] = properties[p]
            else:
                raise OcfClientError(
                        "Error Setting properties "
                        + "of '{0}': Invalid property '{1}'".format(
                            resource, p))
        result = request('PUT', self.url.geturl() + resource, json=result)

        # Error
        if result.status_code >= 400:
            raise OcfClientError(
                    "Error Setting properties of '{0}': {1} ({2})".format(
                        resource, result.reason, result.status_code))
        return(True)

