#!/usr/bin/env python
#

# Imports to make code work on both python 2 & 3
from __future__ import absolute_import, division, print_function
from six import print_ as print
from six.moves import input

# Imports required by the current script
import sys
import os
import re
import json
from ocf_client import OcfClient

# Logging
import logging
logging.basicConfig(level=logging.WARN)

# Clear screen
if os.name == 'nt':
    os.system('cls')
elif os.name == 'posix':
    try:
        os.system('clear')
    except:
        pass

# Choose URL to communicate with
url = input('OCF server URL: ')
ocf_info = [
    'PlatformInformation',
    'DeviceInformation',
    'ConfigurationInformation',
    'MaintenanceInformation',
    'Resources'
]

print('\nCreate OcfClient to url {} ...'.format(url))
ocf = OcfClient(url=url)
print('Done.')

def GetResources(r='/'):
    retVal = []
    resources = ocf.Get('Resources', r)
    if not resources: return retVal
    for rt in resources:
        for r in rt['links']:
            if re.match(r'/oic/(p|d|con|mnt)$', r['href']): continue
            l = GetResources(r['href'])
            if l:
                for i in l:
                    if re.search(r'/oic/(p|d|con|mnt)$', i): continue
                    retVal.append(i)
            else:
                retVal.append(r['href'])
    return retVal

def GetDevices(r='/'):
    retVal = []
    if ocf.Get('DeviceInformation', r):
        retVal.append(r)
    resources = ocf.Get('Resources', r)
    if not resources: return retVal
    for rt in resources:
        for r in rt['links']:
            l = GetDevices(r['href'])
            if l:
                for i in l: retVal.append(i)
    return retVal

print('\nList of devices:')
for d in GetDevices():
    print('\n{}:'.format(d))
    for info in ocf_info:
        print('\nGet {} ...'.format(info))
        result = ocf.Get(info, d)
        if result:
            print(json.dumps(result, indent=2))
        print('Done.')
print('Done.')

print('\nList of resources:')
for r in GetResources():
    print('\n{}:'.format(r))
    result = ocf.Get(r)
    if result:
        print(json.dumps(result, indent=2))
    print('Done.')
print('Done.')
